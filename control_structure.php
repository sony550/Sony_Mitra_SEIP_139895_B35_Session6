<!--if..elseif..else.. condition-->
<?php
$a=5;
$b=5;
$c="both are equal";
if ($a>$b)
{
    echo $a;
}
elseif($a==$b)
{
    echo $c;
}
else
{
    echo $b;
}
echo "<br>";
?>

<!--switch condition-->
<?php
$a=50;

switch($a)
{
    case 1:
        echo "work case 1";
        break;
    case 50:
        echo "work case 2";
        break;
}
echo "<br>";
?>

<!--while loop-->
<?php
$var=0;
while ($var<10)
{
   if($var%2 == 0)
   {
       echo $var;
   }

    $var++;
}
echo "<br>";
?>

<!--for loop-->
<?php
for ($init=0; $init<10; $init++)
{
    echo $init;
}
echo "<br>";
?>

<?php
$loop=0;
do{
    echo "Hello";
}
while ($loop>5);
?>

<?php
$colors = array("red", "green", "orange");

foreach ($colors as $key=>$value)
{

    echo "$key <br>";
    echo "$value <br>";


}


